#pragma once
#include "afxwin.h"


// CSettingDlg ダイアログ

class CSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CSettingDlg)

public:
	CSettingDlg(CWnd* pParent = NULL);   // 標準コンストラクター
	virtual ~CSettingDlg();

// ダイアログ データ
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_SETTING };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
private:
	CComboBox m_cmbLanguage;
	CComboBox m_cmbSpeed;
	CEdit m_editLong;
	CEdit m_editShort;
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnChangeEditLong();
	afx_msg void OnChangeEditShort();
};
