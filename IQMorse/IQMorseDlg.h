
// IQMorseDlg.h : ヘッダー ファイル
//

#pragma once

#include "IQMorseTable.h"

// CIQMorseDlg ダイアログ
class CIQMorseDlg : public CDialogEx
{
// コンストラクション
public:
	CIQMorseDlg(CWnd* pParent = NULL);	// 標準コンストラクター

// ダイアログ データ
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IQMORSE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV サポート


// 実装
protected:
	HICON m_hIcon;

	// 生成された、メッセージ割り当て関数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonShort();
	afx_msg void OnBnClickedButtonLong();
private:
	CString m_strCode;
	CString m_strDecode;
	UINT_PTR m_unTimer = 0;
	unsigned int m_unSpeed = 100;
	unsigned int m_unTextLenght = 0;
	unsigned int m_unDecodeLength = 0;
	bool m_bSpace = true;
	int m_nHomeCount = 0;
	bool m_bHome = true;
	IQMorseTable m_table;
	CString m_code = _T("");
	int	m_nLanguage = MORSE_LANG_JAPANESE;
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();
	afx_msg void OnBnClickedButtonSetting();
private:
	void resetTimer(void);
	void keyLong(void);
	void keyShort(void);
	void setScrollPosBottom(void);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonClear();
	IQMorseTable*	getTable(void) { return &m_table; }
};
