#pragma once

#define MORSE_LANG_JAPANESE	1
#define MORSE_LANG_ENGLISH	2

#define SECTION_NAME		_T("SETTING")
#define KEY_NAME_LANGUAGE	_T("LANGUAGE")
#define	KEY_NAME_SPEED		_T("SPEED")
#define	KEY_NAME_KEY_LONG	_T("KEY_LONG")
#define	KEY_NAME_KEY_SHORT	_T("KEY_SHORT")

#define LANGUAGE_JAPANESE	_T("JAPANESE")
#define LANGUAGE_ENGLISH	_T("ENGLISH")

#define DEFAULT_LANGUAGE	MORSE_LANG_JAPANESE
#define DEFAULT_SPEED		100
#define	DEFAULT_KEY_LONG	('z')
#define	DEFAULT_KEY_SHORT	('x')

class IQMorseSetting
{
public:
	IQMorseSetting();
	~IQMorseSetting();

	void loadSettings(void);
	void saveSettings(void);

public:
	int				m_nLanguage = DEFAULT_LANGUAGE;
	unsigned int	m_unSpeed = DEFAULT_SPEED;
	char			m_cKeyLong = DEFAULT_KEY_LONG;
	char			m_cKeyShort = DEFAULT_KEY_SHORT;

private:
	CString			m_IniFileName = _T("");

};

