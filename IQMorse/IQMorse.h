
// IQMorse.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです。
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CIQMorseApp:
// このクラスの実装については、IQMorse.cpp を参照してください。
//

class CIQMorseApp : public CWinApp
{
public:
	CIQMorseApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CIQMorseApp theApp;