// SettingDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "IQMorse.h"
#include "SettingDlg.h"
#include "afxdialogex.h"
#include "IQMorseDlg.h"


// CSettingDlg ダイアログ

IMPLEMENT_DYNAMIC(CSettingDlg, CDialog)

CSettingDlg::CSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_DIALOG_SETTING, pParent)
{

}

CSettingDlg::~CSettingDlg()
{
}

void CSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_LANGUAGE, m_cmbLanguage);
	DDX_Control(pDX, IDC_COMBO_SPEED, m_cmbSpeed);
	DDX_Control(pDX, IDC_EDIT_LONG, m_editLong);
	DDX_Control(pDX, IDC_EDIT_SHORT, m_editShort);
}


BEGIN_MESSAGE_MAP(CSettingDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSettingDlg::OnBnClickedOk)
	ON_EN_CHANGE(IDC_EDIT_LONG, &CSettingDlg::OnChangeEditLong)
	ON_EN_CHANGE(IDC_EDIT_SHORT, &CSettingDlg::OnChangeEditShort)
END_MESSAGE_MAP()


// CSettingDlg メッセージ ハンドラー


BOOL CSettingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_cmbLanguage.AddString(_T("和文モールス符号"));
	m_cmbLanguage.AddString(_T("欧文モールス符号"));


	m_cmbSpeed.AddString(_T("80ms"));
	m_cmbSpeed.AddString(_T("100ms"));
	m_cmbSpeed.AddString(_T("120ms"));
	m_cmbSpeed.AddString(_T("140ms"));
	m_cmbSpeed.AddString(_T("160ms"));
	m_cmbSpeed.AddString(_T("180ms"));
	m_cmbSpeed.AddString(_T("200ms"));

	//設定の反映
	CIQMorseDlg* parent = (CIQMorseDlg*)m_pParentWnd;
	IQMorseTable* table = parent->getTable();
	IQMorseSetting* setting = table->getSetting();

	if (setting->m_nLanguage == MORSE_LANG_JAPANESE) {
		m_cmbLanguage.SetCurSel(0);
	}
	else {
		m_cmbLanguage.SetCurSel(1);
	}

	switch (setting->m_unSpeed) {
	case 80:
		m_cmbSpeed.SetCurSel(0);
		break;
	case 100:
		m_cmbSpeed.SetCurSel(1);
		break;
	case 120:
		m_cmbSpeed.SetCurSel(2);
		break;
	case 140:
		m_cmbSpeed.SetCurSel(3);
		break;
	case 160:
		m_cmbSpeed.SetCurSel(4);
		break;
	case 180:
		m_cmbSpeed.SetCurSel(5);
		break;
	case 200:
		m_cmbSpeed.SetCurSel(6);
		break;
	default:
		m_cmbSpeed.SetCurSel(1);
		break;
	}
	m_editLong.SetWindowText(CString(setting->m_cKeyLong));
	m_editShort.SetWindowText(CString(setting->m_cKeyShort));

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 例外 : OCX プロパティ ページは必ず FALSE を返します。
}


void CSettingDlg::OnBnClickedOk()
{
	UpdateData();

	//設定の反映
	CIQMorseDlg* parent = (CIQMorseDlg*)m_pParentWnd;
	IQMorseTable* table = parent->getTable();
	IQMorseSetting* setting = table->getSetting();

	if (m_cmbLanguage.GetCurSel() == 0) {
		//和文
		setting->m_nLanguage = MORSE_LANG_JAPANESE;
	}
	else {
		//欧文
		setting->m_nLanguage = MORSE_LANG_ENGLISH;
	}

	switch (m_cmbSpeed.GetCurSel()) {
	case 0:
		setting->m_unSpeed = 80;
		break;
	case 1:
		setting->m_unSpeed = 100;
		break;
	case 2:
		setting->m_unSpeed = 120;
		break;
	case 3:
		setting->m_unSpeed = 140;
		break;
	case 4:
		setting->m_unSpeed = 160;
		break;
	case 5:
		setting->m_unSpeed = 180;
		break;
	case 6:
		setting->m_unSpeed = 200;
		break;
	default:
		setting->m_unSpeed = 100;
		break;
	}

	CString longStr;
	m_editLong.GetWindowText(longStr);
	if (longStr.GetLength() != 1) {
		setting->m_cKeyLong = 'z';
	}
	else {
		setting->m_cKeyLong = static_cast<char>(longStr.GetAt(0));
	}

	CString shortStr;
	m_editShort.GetWindowText(shortStr);
	if (shortStr.GetLength() != 1) {
		setting->m_cKeyShort = 'x';
	}
	else {
		setting->m_cKeyShort = static_cast<char>(shortStr.GetAt(0));
	}

	CDialog::OnOK();
}


void CSettingDlg::OnChangeEditLong()
{
	CString longStr;
	m_editLong.GetWindowText(longStr);
	if (longStr.GetLength() > 1) {
		longStr = longStr.Right(1);
		m_editLong.SetWindowText(longStr);
		m_editLong.SetSel(1, 1);
	}
}


void CSettingDlg::OnChangeEditShort()
{
	CString shortStr;
	m_editShort.GetWindowText(shortStr);
	if (shortStr.GetLength() > 1) {
		shortStr = shortStr.Right(1);
		m_editShort.SetWindowText(shortStr);
		m_editShort.SetSel(1, 1);
	}
}
