#include "stdafx.h"
#include "IQMorseTable.h"


IQMorseTable::IQMorseTable()
{
	mapInitEnglish();
	mapInitJapanese();
}


IQMorseTable::~IQMorseTable()
{
}

CString IQMorseTable::decodeEnglish(CString & code)
{
	std::map<CString, CString>::iterator it = m_mapMorseEnglish.find(code);
	if (it == m_mapMorseEnglish.end()) {
		//見つからない
		return _T("■");
	}
	return it->second;
}
CString IQMorseTable::decodeJapanese(CString & code)
{
	std::map<CString, CString>::iterator it = m_mapMorseJapanese.find(code);
	if (it == m_mapMorseJapanese.end()) {
		//見つからない
		return _T("■");
	}
	return it->second;
}

void IQMorseTable::mapInitEnglish(void)
{
	m_mapMorseEnglish.clear();
	int num = sizeof(_morseEnglish) / sizeof(morseDef);
	for (int i = 0; i < num; i++) {
		CString code = _morseEnglish[i].morse;
		CString decode = _morseEnglish[i].decode;
		m_mapMorseEnglish.insert(std::pair<CString, CString>(code, decode));
	}
}

void IQMorseTable::mapInitJapanese(void)
{
	m_mapMorseJapanese.clear();
	int num = sizeof(_morseJapanese) / sizeof(morseDef);
	for (int i = 0; i < num; i++) {
		CString code = _morseJapanese[i].morse;
		CString decode = _morseJapanese[i].decode;
		m_mapMorseJapanese.insert(std::pair<CString, CString>(code, decode));
	}
}
