//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// IQMorse.rc で使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IQMORSE_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_LONG                 130
#define IDB_BITMAP_SHORT                131
#define IDD_DIALOG_SETTING              132
#define IDC_EDIT_CODE                   1000
#define IDC_EDIT_DECODE                 1001
#define IDC_BUTTON_CLEAR                1002
#define IDC_BUTTON_SETTING              1003
#define IDC_BUTTON_LONG                 1004
#define IDC_BUTTON_SHORT                1005
#define IDC_COMBO_LANGUAGE              1008
#define IDC_COMBO_SPEED                 1009
#define IDC_EDIT_LONG                   1011
#define IDC_EDIT_SHORT                  1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
