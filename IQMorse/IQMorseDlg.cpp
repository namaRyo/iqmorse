
// IQMorseDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "IQMorse.h"
#include "IQMorseDlg.h"
#include "afxdialogex.h"
#include "SettingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// アプリケーションのバージョン情報に使われる CAboutDlg ダイアログ

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// ダイアログ データ
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

// 実装
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CIQMorseDlg ダイアログ



CIQMorseDlg::CIQMorseDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_IQMORSE_DIALOG, pParent)
	, m_strCode(_T(""))
	, m_strDecode(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CIQMorseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_CODE, m_strCode);
	DDX_Text(pDX, IDC_EDIT_DECODE, m_strDecode);
}

BEGIN_MESSAGE_MAP(CIQMorseDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_SHORT, &CIQMorseDlg::OnBnClickedButtonShort)
	ON_BN_CLICKED(IDC_BUTTON_LONG, &CIQMorseDlg::OnBnClickedButtonLong)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_SETTING, &CIQMorseDlg::OnBnClickedButtonSetting)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR, &CIQMorseDlg::OnBnClickedButtonClear)
END_MESSAGE_MAP()


// CIQMorseDlg メッセージ ハンドラー

BOOL CIQMorseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// "バージョン情報..." メニューをシステム メニューに追加します。

	// IDM_ABOUTBOX は、システム コマンドの範囲内になければなりません。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定

	// TODO: 初期化をここに追加します。
	CButton* btn = (CButton*)GetDlgItem(IDC_BUTTON_LONG);
	CBitmap bmp;
	bmp.LoadBitmap(IDB_BITMAP_LONG);
	btn->SetBitmap(bmp);

	btn = (CButton*)GetDlgItem(IDC_BUTTON_SHORT);
	CBitmap bmp2;
	bmp2.LoadBitmap(IDB_BITMAP_SHORT);
	btn->SetBitmap(bmp2);

	//キャレットを隠す
	GetDlgItem(IDC_EDIT_CODE)->HideCaret();
	GetDlgItem(IDC_EDIT_DECODE)->HideCaret();


	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

void CIQMorseDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void CIQMorseDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// クライアントの四角形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンの描画
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR CIQMorseDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CIQMorseDlg::OnBnClickedButtonShort()
{
	keyShort();
}



void CIQMorseDlg::OnBnClickedButtonLong()
{
	keyLong();
}


void CIQMorseDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (!m_bHome)
	{
		if (!m_bSpace)
		{
			m_bSpace = true;
			m_strCode += _T("　");
			CString decode;
			if (m_nLanguage == MORSE_LANG_JAPANESE) {
				decode = m_table.decodeJapanese(m_code);
			}
			else {
				decode = m_table.decodeEnglish(m_code);
			}
			m_code.Empty();
			m_strDecode += decode;
			m_unTextLenght++;
			if (m_unTextLenght > 24)
			{
				m_strCode += "\r\n";
				m_unTextLenght = 0;
				UpdateData(FALSE);
				setScrollPosBottom();
			}
			m_unDecodeLength++;
			if (m_unDecodeLength > 24)
			{
				m_strDecode += "\r\n";
				m_unDecodeLength = 0;
				UpdateData(FALSE);
				setScrollPosBottom();
			}

		}
		else
		{
			m_nHomeCount++;
			if (m_nHomeCount > 2) {
				m_nHomeCount = 0;
				m_bHome = true;
				m_strCode += "\r\n";
				m_unTextLenght = 0;
				m_strDecode += _T(" ");
				m_unDecodeLength++;
				if (m_unDecodeLength > 24)
				{
					m_strDecode += "\r\n";
					m_unDecodeLength = 0;
				}
				UpdateData(FALSE);
				setScrollPosBottom();
			}
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CIQMorseDlg::OnClose()
{
	KillTimer(m_unTimer);
	CDialogEx::OnClose();
}


void CIQMorseDlg::OnBnClickedButtonSetting()
{
	CSettingDlg dlg(this);
	if (dlg.DoModal() == IDOK)
	{
		//設定反映
		m_unSpeed = m_table.getSetting()->m_unSpeed;
		m_nLanguage = m_table.getSetting()->m_nLanguage;
	}
}

void CIQMorseDlg::resetTimer(void)
{
	m_unTimer = SetTimer(1, m_unSpeed, nullptr);
}

void CIQMorseDlg::keyLong(void)
{
	Beep(880, m_unSpeed * 3);
	resetTimer();
	m_bSpace = false;
	m_bHome = false;
	m_nHomeCount = 0;
	m_unTextLenght++;
	m_strCode += "−";
	m_code += "-";
	UpdateData(FALSE);
	setScrollPosBottom();
}

void CIQMorseDlg::keyShort(void)
{
	Beep(880, m_unSpeed);
	resetTimer();
	m_bSpace = false;
	m_bHome = false;
	m_nHomeCount = 0;
	m_unTextLenght++;
	m_strCode += "・";
	m_code += ".";
	UpdateData(FALSE);
	setScrollPosBottom();
}

void CIQMorseDlg::setScrollPosBottom(void)
{
	//スクロール位置を一番下へ
	CEdit* edit = (CEdit*)GetDlgItem(IDC_EDIT_DECODE);
	CString str;
	edit->GetWindowText(str);
	int len = str.GetLength();
	edit->SetSel(len, len, FALSE);

	edit = (CEdit*)GetDlgItem(IDC_EDIT_CODE);
	edit->GetWindowText(str);
	len = str.GetLength();
	edit->SetSel(len, len, FALSE);
}


BOOL CIQMorseDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_CHAR){
		unsigned int keyCode = (unsigned int)pMsg->wParam;
		if (keyCode == m_table.getSetting()->m_cKeyLong)
		{
			keyLong();
			return TRUE;
		}
		else if (keyCode == m_table.getSetting()->m_cKeyShort)
		{
			keyShort();
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CIQMorseDlg::OnBnClickedButtonClear()
{
	m_strCode.Empty();
	m_strDecode.Empty();
	UpdateData(FALSE);
}
