#include "stdafx.h"
#include "IQMorseSetting.h"
#include "IQMorse.h"


IQMorseSetting::IQMorseSetting()
{
	loadSettings();
}


IQMorseSetting::~IQMorseSetting()
{
	saveSettings();
}

void IQMorseSetting::loadSettings(void)
{
	CIQMorseApp* pApp = (CIQMorseApp*)AfxGetApp();
	//言語
	CString str = pApp->GetProfileString(SECTION_NAME, KEY_NAME_LANGUAGE);
	if (!str.IsEmpty()) {
		if (str.Compare(LANGUAGE_JAPANESE) == 0) {
			m_nLanguage = MORSE_LANG_JAPANESE;
		}
		else {
			m_nLanguage = MORSE_LANG_ENGLISH;
		}
	}
	//速度
	m_unSpeed = pApp->GetProfileInt(SECTION_NAME, KEY_NAME_SPEED, DEFAULT_SPEED);
	//長音
	m_cKeyLong = (char) pApp->GetProfileInt(SECTION_NAME, KEY_NAME_KEY_LONG, DEFAULT_KEY_LONG);
	//短音
	m_cKeyShort = (char)pApp->GetProfileInt(SECTION_NAME, KEY_NAME_KEY_SHORT, DEFAULT_KEY_SHORT);
}

void IQMorseSetting::saveSettings(void)
{
	CIQMorseApp* pApp = (CIQMorseApp*)AfxGetApp();
	//言語
	pApp->WriteProfileString(SECTION_NAME, KEY_NAME_LANGUAGE,
		m_nLanguage == MORSE_LANG_JAPANESE ? LANGUAGE_JAPANESE : LANGUAGE_ENGLISH);
	//速度
	pApp->WriteProfileInt(SECTION_NAME, KEY_NAME_SPEED, m_unSpeed);
	//長音
	pApp->WriteProfileInt(SECTION_NAME, KEY_NAME_KEY_LONG, m_cKeyLong);
	//短音
	pApp->WriteProfileInt(SECTION_NAME, KEY_NAME_KEY_SHORT, m_cKeyShort);
}
